# -*- encoding: utf-8 -*-

import socket
import email.utils
import os
import time


def http_serve(server_socket):
    """

    :param server_socket:
    :param html:
    """
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                print "Odebrano:"
                print request
                data=bytes.decode(request)
                request_method=data.split(' ')[0]
                print(request_method)

                if(request_method=='GET')|(request_method=='HEAD'):
                    file_requested=data.split(' ')
                    file_requested=file_requested[1]
                    if(file_requested=='/'):
                        file_requested='/web_page.html'
                    file_requested='web'+file_requested
                    print(file_requested)
                    try:
                        if(request_method=='GET'):
                            response_headers='HTTP/1.1 200 OK \n'
                            if(file_requested.endswith('.html'))|(file_requested.endswith('.htm')):
                                response_content=open(file_requested,'rb').read()
                                response_headers+="Content-Type: text/html \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                            elif(file_requested.endswith('.jpg'))|(file_requested.endswith('.jpeg')):
                                response_content=file(file_requested,'rb').read()
                                response_headers+="Content-Type: image/jpg \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                            elif(file_requested.endswith('.png')):
                                response_content=file(file_requested,'rb').read()
                                response_headers+="Content-Type: image/jpg \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                            elif(file_requested.endswith(".txt")):
                                response_content=file(file_requested).read()
                                response_headers+="Content-Type: text/plain; charset=UTF-8\r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                            elif os.path.exists(file_requested) and os.path.isdir(file_requested):
                                #zabawa w htmlu
                                response_content='<html><head></head><body>'
                                directory_list=os.listdir(file_requested)
                                print(directory_list)
                                for name in directory_list:
                                    if os.path.isdir(file_requested):
                                        response_content+='<a href="'+file_requested.replace('web/','/')+'/'+name+'">'+'/'+name+'</a>'+'<br></br>'
                                        print (name)
                                    else:
                                        response_content+='<a href="'+file_requested.replace('web/','/')+'/'+name+'">'+'/'+name+'</a>'+'<br></br>'
                                response_content+='</body></html>'
                                response_headers+="Content-Type: text/html; charset=UTF-8\r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                            else:
                                response_headers='HTTP/1.1 404 Not Found  \r\n'
                                if(request_method=='GET'):
                                    response_content='<html><body> Error 404 bla bla </body></html>'
                                    response_headers+="Content-Type: text/html \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                                server_response=response_content
                    except Exception as e:
                        print("File was not found")
                        response_headers='HTTP/1.1 404 Not Found  \r\n'
                        if(request_method=='GET'):
                            response_content='<html><body> Error 404 bla bla </body></html>'
                            response_headers+="Content-Type: text/html \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                    server_response=response_content
                    print("zalatwiony")
                else:
                    response_headers='HTTP/1.1 400 Bad Request  \r\n'
                    response_content='<html><body> Error 404 bla bla </body></html>'
                    response_headers+="Content-Type: text/html \r\n"+"Content-Length: "+str(len(response_content))+"\r\n"
                    server_response=response_content
                    print("Unknown HTTP request method: ",request_method +"jakis cwaniak")
            connection.send(response_headers+"\r\n"+ server_response)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
#server_address = ('localhost', 80)  # TODO: zmienić port!
server_address=('194.29.175.240',31013)
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

#html = open('web/web_page.html').read()

try:
    http_serve(server_socket)

finally:
    server_socket.close()

